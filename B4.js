/**
 * Input: Chiều dài và chiều rộng
 *
 * Step:
 * -B1: Tạo 2 biến chứa chiều dài và chiều rộng
 * -B2: Tạo 2 biến chứa kết quả diện tích và chu vi
 * -B3: Tính chu vi và diện tích theo công thức
 *      + chuVi = (dài + rộng)*2
 *      + dienTich = dài*rộng
 * -B4: In kết quả ra màn hình
 *
 *
 * Output: Chu vi và diện tích
 */
var tinhToan = function () {
  var width = document.getElementById("width").value * 1;
  var height = document.getElementById("height").value * 1;
  var chuVi = (width + height) * 2;
  var dienTich = width * height;
  console.log({ chuVi }, { dienTich });
  document.getElementById(
    "hcn"
  ).innerText = `Chu vi: ${chuVi}   Diện tích: ${dienTich}`;
};
