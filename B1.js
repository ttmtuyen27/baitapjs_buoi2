/*
Input: Số ngày làm (vd: 5 ngày)
Step:
-B1: Tạo 1 biến chứa số ngày làm và 1 biến chứa tiền lương
-B2: Tính tiền lương theo công thức: lương = số ngày*100000
-B3: In kết quả tiền lương ra màn hình
Output: Tiền lương 
*/
var tinhLuong = function () {
  var soNgay = document.getElementById("numOfDay").value * 1;
  var tienLuong = null;
  tienLuong = soNgay * 100000;
  console.log(tienLuong);
  document.getElementById(
    "salary"
  ).innerHTML = `Tiền lương của bạn là: ${tienLuong}vnd`;
};
