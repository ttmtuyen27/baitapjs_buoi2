/**
 * Input: Nhập số tiền theo USD
 *
 * Step:
 * -B1: Tạo 1 biến chứa giá trị USD
 * -B2: Tạo 1 biến chứa giá trị chuyển đổi ra VND
 * -B3: Tính giá trị chuyển đổi theo công thức: VND=USD*23500
 * -B4:In kết quả ra màn hình
 *
 * Output: Số tiền chuyển đổi ra VND
 */

var doiTienTe = function () {
  var usd = document.getElementById("usd").value;
  var vnd = null;
  vnd = usd * 23500;
  console.log("VND: ", vnd);
  document.getElementById("vnd").innerHTML = `Số tiền: ${vnd} VND`;
};
