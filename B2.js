/**
 * Input: 5 số thực
 *
 * Step:
 * -B1: tạo 5 biến chứa 5 giá trị input
 * -B2: tạo biến chứa kết quả
 * -B3: tính giá trị trung bình theo công thức: giaTriTrungBinh=(num1+num2+num3+num4+num5)/5
 * -B4: In kết quả trung bình ra màn hình
 *
 * Output: giá trị trung bình
 *
 */
var tinhTrungBinh = function () {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var num4 = document.getElementById("num4").value * 1;
  var num5 = document.getElementById("num5").value * 1;
  var result = (num1 + num2 + num3 + num4 + num5) / 5;
  console.log(result);
  document.getElementById(
    "result"
  ).innerText = `Trung bình cộng của 5 số là: ${result}`;
};
